$(document).ready(SetUp);

function SetUp() {
	$("button").click(getCompanies);

	$("#services").change( function(){
			getCitiesByServiceId($(this).val());
	});

}


function getCompanies() {
	var cityId = $("#cities").val();
	var serviceId = $("#services").val();
	
	var url = "http://localhost:8080/VirtualConcierge/home/list?serviceId="+serviceId+"&cityId="+cityId;
	ajaxGet(url, displayList);
}

function displayList(result) {
	$('#lista').html(result);
}



function getCitiesByServiceId(id) {
	var url = "http://localhost:8080/VirtualConcierge/home/getCities?serviceId="+id;
	ajaxGet(url, displayCities);
}

function displayCities(result) {
	 $('#cities').empty();
	 $('#cities').append($('<option disabled selected>').text("Select City").attr('value', ""));
	 $.each(result, function(i, city) {
		 $('#cities').append($('<option>').text(city.name).attr('value', city.id));
	 });
}

function ajaxGet(url, inputFunction) {
	$.ajax({
		url : url,
		type : 'GET'
	}).done(inputFunction);
}
